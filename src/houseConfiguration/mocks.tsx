import {
    homeIcon,
    tvSetIcon,
    kitchenIcon,
    bedIcon,
    bathIcon,
    carIcon
} from '../shared/icons';
import { RoomConfig } from '../shared/interfaces/DTOs/room-config';

export const MyHouse: Array<RoomConfig> = [
    {
        name: 'Global',
        id: '01',
        icon: homeIcon()
    },
    {
        name: 'Living Room',
        id: '02',
        icon: tvSetIcon()
    },
    {
        name: 'Kitchen',
        id: '03',
        icon: kitchenIcon()
    },
    {
        name: 'Sleeping Room 1',
        id: '04',
        icon: bedIcon()
    },
    {
        name: 'Sleeping Room 2',
        id: '05',
        icon: bedIcon()
    },
    {
        name: 'BathRoom',
        id: '06',
        icon: bathIcon()
    },
    {
        name: 'Garage',
        id: '07',
        icon: carIcon()
    }
];