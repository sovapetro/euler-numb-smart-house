import React, { Component, ReactNode } from 'react';
import { BrandName, NavLinks } from './components';

class Header extends Component {
    render(): ReactNode {
        return (
            <div className='header'>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <BrandName />
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav">
                           <NavLinks />
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default Header;

// todo: settings(configuration) allow to configure app
// 1. Settup numbers of rooms, squares required devices
// 2. Default settings for entire house
// 3. use dynamic load via import() syntax