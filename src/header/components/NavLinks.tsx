import React, { Component, ReactNode, Fragment } from 'react';

export class NavLinks extends Component {
    render(): ReactNode {
        return (
            <Fragment>
                <a className="nav-item nav-link active" href="#Home">Home</a>
                <a className="nav-item nav-link" href="#Features">Features</a>
                <a className="nav-item nav-link" href="#Pricing">Pricing</a>
                <a className="nav-item nav-link disabled" href="#Disabled">Disabled</a>
            </Fragment>
        );
    }
}
