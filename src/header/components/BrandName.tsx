import React, { Component, ReactNode, Fragment } from 'react';
import ConfigIcon from './HouseConfig';

export class BrandName extends Component {
    render(): ReactNode {
        import('../../shared/some.lib').then(lib => lib.salo())
        return (
            <Fragment>
                <ConfigIcon />
                <span className="navbar-brand">Euler's House</span>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
            </Fragment>
        );
    }
}

