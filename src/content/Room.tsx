import React, { Component, ReactNode} from 'react';

import './Content.sass';

type Props = {
    roomConfig: any;
};

type State = {
    roomName: string;
    icon: ReactNode;
};

function onClickHandler() {
    console.log('sasasasasa');
}

export class Room extends Component<Props, State> {

    constructor(props : Props) {
        super(props);
        this.state = {
            roomName: this.props.roomConfig.name,
            icon: this.props.roomConfig.icon
        };
    }
    render(): ReactNode {
        return (
            <div className='room' onClick={onClickHandler}>
                {this.state.icon}
                <br />
                {this.state.roomName}
            </div>
        );
    }
}
