import React, { Component, ReactNode } from 'react';
import { Room } from './Room';
import './Content.sass';
import { MyHouse } from '../houseConfiguration/mocks';
import { RoomConfig } from '../shared/interfaces/DTOs/room-config';

class Content extends Component {
    render(): ReactNode {
        const element: ReactNode = createRoomsFromConfig(MyHouse);
        return (
            <div className='content'>
                <div className='flex-parent'>
                    {element}
                </div>
            </div>
        )
    }
}

function createRoomsFromConfig(rooms : Array<RoomConfig>) : ReactNode {
    return rooms.map(room => {
        return (<div className='flex-child' key={room.id}>
            <Room roomConfig={room} />
        </div>);
    });
}

export default Content;