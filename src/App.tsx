import React, { Component, ReactNode } from 'react';

import './App.css';
// import FirstComponent from './common/First-component';
import Content from './content/Content';
import Footer from './footer/Footer';
import Header from './header/Header';

class EulerNumbSmartHouse extends Component {
    render(): ReactNode {
        return (
            <div className="App">
                <Header />
                <Content />
                <Footer />
            </div>
        );
    }

}

type Props = {
    count?: number;
}

type State = {
    isToggleOn?: boolean;
}

// className Toggle extends Component<Props, State> {
//   constructor(props : any) {
//     super(props);
//     this.state = {isToggleOn: true};

//     // This binding is necessary to make `this` work in the callback
//     this.handleClick = this.handleClick.bind(this);
//   }

//   handleClick = () => {
//     this.setState((state : any) => ({
//       isToggleOn: !state.isToggleOn
//     }));
//   }

//   render() {
//     return (
//       <button onClick={this.handleClick}>
//         {this.state.isToggleOn ? 'ON' : 'OFF'}
//       </button>
//     );
//   }
// }


export default EulerNumbSmartHouse;
