import React, { Component, ReactNode } from 'react';

class FirstComponent extends Component {

    render(): ReactNode {
        return (
            <div className="First">
                <p> First Component </p>
            </div>
        );
    }

}

export default FirstComponent;