import { ReactNode } from 'react';

export interface RoomConfig {
    name: string;
    icon: ReactNode;
    id?: string;
};